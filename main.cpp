#include <iostream>
#include <string>
#include "lsdiff.h"

using namespace std;


/*
 * usage:

 lsbindiff file1 file2 -f 64/32/16/8 -p list/csv/xml -


 */



void printUsage(){
    cout<<"usage:"<<"lsbindiff file1 file2 -f hex8/hex16/hex32/s8/s16/s32/u8/u16/u32 -p raw/csv/xml -o filename"<<endl;
}
int checkParams(lsdiff::params*pp, int argc, char *argv[]){

    //first two mandatory params: file1 & file2
    if(argc<3)
    {
        return -1;
    }
    else
    {
        pp->file1=argv[1];
        pp->file2=argv[2];
    }

    string argstr;

    int argi;
    argi=3;
    while(argi<argc)
    {
        argstr.append(argv[argi]);

        //-f: format
        if(argstr=="-f")
        {
            argi++;
            if(argi>=argc)
            {
                return -1;
            }
            argstr.clear();

            argstr.append(argv[argi]);

            if(argstr=="x8")
                pp->df=lsdiff::hex8;
            else if(argstr=="x16")
                pp->df=lsdiff::hex16;
            else if(argstr=="x32")
                pp->df=lsdiff::hex32;
            else if(argstr=="s8")
                pp->df=lsdiff::sInt8;
            else if(argstr=="s16")
                pp->df=lsdiff::sInt16;
            else if(argstr=="s32")
                pp->df=lsdiff::sInt32;
            else if(argstr=="u8")
                pp->df=lsdiff::uInt8;
            else if(argstr=="u16")
                pp->df=lsdiff::uInt16;
            else if(argstr=="u32")
                pp->df=lsdiff::uInt32;
            else
                return -1;
            argi++;
        }


    }

    //argstr.append(argv[3]);




    return 0;

}



int main(int argc, char *argv[])
{
    //cout << "Hello World!" << argc<<endl;

    lsdiff::params pp;
    pp.df=lsdiff::hex8;
    pp.of=lsdiff::raw;

    if(checkParams(&pp,argc,argv))
    {
        printUsage();
        return 1;
    }
    //cout << file1 << file2 << endl;
    lsdiff lsd(pp);

    int res;
    res=lsd.rundiff();
    lsd.output();
    if(res<0)
    {
        cout<<"diff had an error"<<endl;
    }
    else if(res==0)
    {
        cout <<"files are same"<<endl;
    }
    else
    {
        cout <<"found "<<res <<" differences"<<endl;
    }
    return res;
}
