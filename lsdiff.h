#ifndef LSDIFF_H
#define LSDIFF_H

#include <iostream>
#include <string>
#include <list>
#include <cstring>
using namespace std;

class lsdiff
{

    #define MAX_READ_BYTES  8






public:

    enum dataformat
    {
        hex8,
        hex16,
        hex32,
        hex64,
        uInt8,
        uInt16,
        uInt32,
        uInt64,
        sInt8,
        sInt16,
        sInt32,
        sInt64,
    };

    enum outformat
    {
        raw,
        csv,
        xml
    };
    enum outTo
    {
        standardout,
        fileout
    };

    struct diff{
        size_t offset;
        uint8_t val1[MAX_READ_BYTES];
        uint8_t val2[MAX_READ_BYTES];
    };

    struct params
    {
        string file1;
        string file2;
        dataformat df;
        outformat of;
    };


    lsdiff(params p);

    int rundiff();
    void output();


private:
  //  string file1;
  //  string file2;
  //  outformat of;
  //  dataformat df;

    params pp;
    std::list<diff> difflist;

    string getvalstr(void*val, dataformat f);



};

#endif // LSDIFF_H
