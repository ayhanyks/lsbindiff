#include "lsdiff.h"

lsdiff::lsdiff(params p)
{

    pp=p;

}



string lsdiff::getvalstr(void*val, dataformat f){
    char strdat[256]={0};
    string outstr;

    outstr.clear();

    switch(f)
    {
    case hex8:
        sprintf(strdat,"%02x",*(uint8_t*)val);
        break;


    case hex16:
        sprintf(strdat,"%04x",*(uint16_t*)val);
        break;

    case hex32:
        sprintf(strdat,"%08x",*(uint32_t*)val);
        break;

    case uInt8:
       sprintf(strdat,"%03u",*(uint8_t*)val);
       break;

    case uInt16:
       sprintf(strdat,"%05u",*(uint16_t*)val);
       break;

     case uInt32:
        sprintf(strdat,"%010u",*(uint32_t*)val);
        break;

    case sInt8:
       sprintf(strdat,"%03d",*(int8_t*)val);
       break;

    case sInt16:
       sprintf(strdat,"%05d",*(int16_t*)val);
       break;

     case sInt32:
        sprintf(strdat,"%010d",*(int32_t*)val);
        break;

    }
    outstr.append(strdat);
    //cout<<outstr<<".";

    return outstr;

}

void lsdiff::output()
{
    list<diff>::iterator it;
    char str[256];

    for(it=difflist.begin(); it!=difflist.end(); ++it)
    {
        diff d=(diff)*it;
        string s;


        s.append(getvalstr((void*)(&d.offset),uInt32));
        s.append("\t");
        s.append(getvalstr(d.val1,pp.df));
        s.append("\t");
        s.append(getvalstr(d.val2,pp.df));
        s.append("\n");

        cout<<s;
    }



}




int lsdiff::rundiff(){
    //open both files.
    FILE*f1;
    FILE*f2;

    f1=fopen(pp.file1.data(), "rb");
    if(!f1)
    {
        return -1;
    }

    f2=fopen(pp.file2.data(), "rb");
    if(!f2)
    {
        return -1;
    }

    uint64_t readdat64;
    uint32_t readdat32;

    uint8_t readdat1[MAX_READ_BYTES];
    uint8_t readdat2[MAX_READ_BYTES];

    uint8_t nrb=0;

    switch (pp.df) {
    case hex8:
    case sInt8:
    case uInt8:
        nrb=1;
        break;
    case hex16:
    case sInt16:
    case uInt16:
        nrb=2;
        break;
    case hex32:
    case sInt32:
    case uInt32:
        nrb=4;
        break;
    case hex64:
    case sInt64:
    case uInt64:
        nrb=8;
        break;
    default:
        nrb=1;
        break;
    }

    size_t offset=0;
    while(!feof(f1) && !feof(f2))
    {


        memset((void*)readdat1,0,MAX_READ_BYTES);
        memset((void*)readdat2,0,MAX_READ_BYTES);


        fread((void*)readdat1,nrb,1,f1);
        fread((void*)readdat2,nrb,1,f2);

        if(memcmp((void*)readdat1,(void*)readdat2,nrb))
        {
            //non matching.
            diff d;
            d.offset=offset;
            memcpy((void*)d.val1,(void*)readdat1,nrb);
            memcpy((void*)d.val2,(void*)readdat2,nrb);

            difflist.push_back(d);
        }


        offset+=nrb;

    }
    //cout<<"difflist size:"<<difflist.size();




    fclose(f1);
    fclose(f2);
    return difflist.size();
}
